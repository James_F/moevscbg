//Name: James Felber
//Section: D
//Date: 11-18-2013
//Description: Header file for driver for hw10

#ifndef DHEADER_H
#define DHEADER_H

#include <string>
#include <fstream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "customer.h"
using namespace std;

const int POPULATION = 20; //relevant population of Springfield
const string BARINVEN = "rum.txt"; //Moe's inventory
const string CBINVEN = "literature.txt"; //CBG's inventory
const string CENSUS = "citizens.dat"; //names & preferences
const short COMMIT = 10; //minimum happiness to stay
const short ASCEND =90;  //maximum happiness to stay


//Desc: Adds customers to an array from a file
//Pre: the array must be >= POPULATION. POPULATION must be equal to
//     the number of entries in the file 
//     and the file must be formated "name, preference" with
//     preference being either a -1 or a 1
//Post: adds customers from file to street[]
void cutmhtgtss(const string file, customer street[]); 
//yes, that is an abbreviation

//Desc: Outputs customers from an array
//Pre: pop must be <= the size of sesame
//Post: Outputs the first "pop" customers from "sesame" to the screen
void survivors(const customer sesame[], short pop);

#endif
