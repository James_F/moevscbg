//Name: James Felber
//Section: D
//Date: 11-18-2013
//Description: Header file for product struct

#ifndef PRODUCT_H
#define PRODUCT_H

#include <string>
using namespace std;

struct product
{
  string m_item;
  float  m_price;

  product(const string item = "", const float price = 0.0): 
    m_item(item), m_price(price){}
};

#endif