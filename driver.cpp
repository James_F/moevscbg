//Name: James Felber
//Section: D
//Date: 11-6-2031
//Desc: Driver for hw9: tests classes "customer" and "business"

#include "business.h"
#include "customer.h"
#include "dheader.h"


int main()
{

  customer sesame[POPULATION];
  business Moes(BARINVEN, "Moe's Tavern",20000);
  business cbg(CBINVEN, "Android's Dungeon & Baseball Card Shop");
  short count = 0, pop = POPULATION, drunks = 0; //tracking variables
  short vic;
  bool repeat = false;
  customer winner;

  srand(time(NULL));

  cout.setf(ios::fixed);
  cout.setf(ios::showpoint);
  cout.precision(2);

  cutmhtgtss(CENSUS,sesame);

  survivors(sesame,pop);


  do
  {
    for (int i = 0; i<pop; i++)
    {
      if(!(sesame[i].fav())) //! to fix a logic error I can't find
      {
        Moes.AddCustomer(sesame[i]);
        drunks++;
      }
      else
        cbg.AddCustomer(sesame[i]);
    }

    cbg.sell_stuff();
    Moes.sell_stuff();

    Moes.customers_leave(sesame,0);
    cbg.customers_leave(sesame,drunks);
    drunks =0;

    random_shuffle(&sesame[0],&sesame[pop]);

    for (int i=0;i<pop;i++)
    {
      vic = rand()%pop;
      if(sesame[i].fav()==sesame[vic].fav())
        sesame[i].rob(sesame[vic]);
      else
        sesame[i].chuck(sesame[vic]);
    }

   for (int i=0;i<pop;i++)
   {
     do
     {
       repeat = false;

       if (sesame[i].DrMonroe() < COMMIT)
       {
         cout << sesame[i].name()
              << " has fallen into despair and been committed"
              << " to the Shelbyville House of Desperation \n";
         swap(sesame[i],sesame[pop-1]);
         pop--;
         repeat = true;
       }
       else if (sesame[i].DrMonroe() > ASCEND)
       {
         cout << sesame[i].name()
              << " has reached an enlightened state and gone"
              << " to the Shelbyville House of Desperation"
              << " to mock the idiots that couldn't figure"
              << " out how to do it. \n";
         swap(sesame[i],sesame[pop-1]);
         pop--;
         repeat = true;
       }
     }while(repeat);
   }

//    survivors(sesame,pop);

    count++;
  }while(count < 20 && pop > 1);

  winner.add_cust("No one",0);
  winner.life(-HAPPY_MAX);

  for (int i = 0; i<POPULATION; i++)
    if(sesame[i].DrMonroe() > winner.DrMonroe())
      winner = sesame[i];

  cout << "And the \"winner\" is " << winner.name()
       <<((Moes.cash() > cbg.cash())? " but the real winner is Moe! \n":
         (Moes.cash()==cbg.cash())? ". \n" :
         " but the real winner is The Comic Book Guy! \n");


  return(0);
}
