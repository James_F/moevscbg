//Name:James Felber
//Section: D
//Date:11-6-2013
//Desc: function file for customer class

#include "customer.h"


bool customer::buy_something(const product suggested)
{
  bool success = false;

  if (rand()%SHOPAHOLIC)
  {
    if (suggested.m_price <= m_funds && m_p_num < P_SIZE)
    {
      m_purchases[m_p_num] = suggested;
      m_funds -= suggested.m_price;
      m_vicodin += BUY_YES;
      m_p_num++;
      success = true;
    }
    else
    {
      m_vicodin -= BUY_NO;
//      cout << m_name << "failed to buy "
//           << suggested.m_item << endl; 
    }
  }
  
  return(success);
}

void customer::chuck(customer & victim)
{
  if (m_p_num)
  {
    m_p_num--;
    m_vicodin += THROW_YES;
    victim.life(-HIT);
    cout << m_name << " throws "
         << m_purchases[m_p_num].m_item
         << " at " << victim.name() << endl;
  }
  else m_vicodin -= THROW_NO;

  return;
}

void customer::rob(customer & victim)
{
  product gta;

  if (victim.bags() && m_p_num < P_SIZE)
  {
    gta = victim.stuff(victim.bags()-1);
    m_purchases[m_p_num] = gta;
    m_p_num++;
    victim.addCart(-1);
    victim.life(-ROBBED);
    m_vicodin += STEAL_YES;
    cout << m_name << " steals " 
         << gta.m_item << " from "
         << victim.name() << endl;
  }
  else
    m_vicodin -= STEAL_NO;

  return;
}

ostream & operator << (ostream & stream, const customer & patsy)
{
  stream << patsy.m_name << " has $" 
         << patsy.m_funds << " is " << patsy.m_vicodin
         << "% Happy and has";

  if (patsy.m_p_num)
  {
    for(int i=0; i <patsy.m_p_num;i++)
    {
      cout << " " << patsy.m_purchases[i].m_item;
      if(i != patsy.m_p_num - 1) //formating
      {
        if (i == patsy.m_p_num - 2)
          cout << " and";
        else
          cout << ",";
      }
    }
  }
  else
    cout << " no purchases.";

  return(stream);
}
