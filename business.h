//Name:James Felber
//Section: D
//Date:11-6-2013
//Desc:

#ifndef BUSINESS_H
#define BUSINESS_H

#include <cstdlib>
#include <ctime>
#include <string>
#include <fstream>
#include "customer.h"
#include "product.h"
using namespace std;

const int INVEN_SIZE = 10;
const int OCCUPANCY = 10;

class business
{
  private:
    string m_name;
    float  m_funds;
    product m_stock[INVEN_SIZE];
    customer m_customer[OCCUPANCY];
    short  m_variety; //current size of m_stock
    short  m_occupants; // current size of m_customer
  public:
    //Desc: constructor for business class
    //Pre: inventory needs to refer to a file that exists and contains products
    //     in the format "name $price ".
    //Post: Constructs a business object with default values 
    //      (if needed) and INVENTORY.
    business (const string inventory, const string name = " ", 
      const float money = 0):m_name(name), m_funds(money), 
      m_variety(0), m_occupants(0)
    {
      ifstream fin;
      string item;

      fin.open(inventory.c_str());
      for(int i=0; i<INVEN_SIZE && getline(fin, item, '$');i++) 
      {
        m_stock[i].m_item = item;

        fin >> m_stock[i].m_price;
        fin.ignore();

        m_variety++;
      }

      fin.close();
    }

//Accessors

    //Desc: returns number of different items the business stocks
    //Pre: none
    //Post: returns m_variety
    short stock() const {return(m_variety);}

    //Desc: returns specified item from business' inventory
    //Pre: business must have a product in the specified location
    //Post: returns the item'th element of m_stock
    product shelf(const short item) const {return(m_stock[item]);}

   //Desc: returns store funds
   //Pre: none
   //Post: returns m_funds
   float cash()const{return(m_funds);}

//Mutators

    //Desc: Tracks new customers
    //Pre: none 
    //Post: Adds new customers to m_customer and increments m_occupants if 
    //      m_occupants is <= OCCUPANCY.
    void AddCustomer(const customer & ncust);

    //Desc: Adjusts business' funds
    //Pre: none
    //Post: adds price to m_funds
    void sale(const float price){m_funds+=price; return;}

//Other

    //Desc: Outputs buisness' info
    //Pre: none
    //Post: Outputs buisness's name, current inventory and current customers
    void display() const;

    //Desc: Checks each customer to see if they'll buy something
    //Pre: none
    //Post: calls each customer's buy_something() function with a random
    //      product from the business' inventory. Alters m_funds if needed.
    void sell_stuff();

    //Desc: Ejects all customers into the street
    //Pre: "seasame" must be large enough to hold business' customers + 
    //     "current". "current" must be >= 0 and indicate the current 
    //     size of "seasme" when when the function is called.
    //Post: Adds all customers in m_customer to sesame and sets m_occupants 
    //      to 0
    void customers_leave(customer sesame[],const short current);
};

#endif
