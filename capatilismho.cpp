//Name: James Felber
//Section: D
//Date: 11-26-2013
//Desc: Function file for hw10

#include "dheader.h"

void cutmhtgtss(const string file, customer street[])
{
  ifstream fin;
  fin.open(file.c_str());
  string temp_n;
  float temp_p;

  if(fin)
    for (int i=0; i< POPULATION; i++)
    {
      getline(fin, temp_n, ',');
      fin >> temp_p;
      fin.ignore();

      street[i].add_cust(temp_n,temp_p);
    }
   else
     cout << "Error, cannot open file: " << file;

  return;
}

void survivors(const customer sesame[], short pop)
{
  if(pop)
  {
  cout << "Current Population of Springfield: \n";
  for (int i = 0; i<pop;i++)
    cout << sesame[i] << endl;
  }

  return;
}
