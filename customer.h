//Name:James Felber
//Section: D
//Date:11-6-2013
//Desc: Header file for customer class

#ifndef CUSTOMER_H
#define CUSTOMER_H

#include <iostream>
#include <string>
#include <cstdlib>
#include "product.h"

using namespace std;


const float MONEY_MAX = 250;
const float MONEY_MIN = 4;
const int P_SIZE = 20;    //maximum # of purchases
const short SHOPAHOLIC =2; //odds of buying (SHOPAHOLIC-1)/SHOPAHOLIC
const short BUY_YES = 15;
const short BUY_NO = 10;
const short STEAL_YES = 25;
const short STEAL_NO = 5;
const short ROBBED = 20;
const short THROW_YES = 5;
const short THROW_NO = 25;
const short HIT = 20;
const short HAPPY_MAX = 100;
const short HAPPY_MIN = 0;


class customer
{
  private:
    product m_purchases[P_SIZE];
    short m_vicodin; //happiness
    short m_p_num; //number of purchases
    bool m_favorite; //inclination 0 = bar, 1 = comic shop
    float m_funds; 
    string m_name;
  public:
    //Desc: constructor for customer class
    //Pre: none
    //Post: constructs a customer class with default values
    customer():m_p_num(0)
    {
      m_vicodin = rand()%(HAPPY_MAX-HAPPY_MIN) + HAPPY_MIN;
      m_funds = rand()%(static_cast<int>(MONEY_MAX - MONEY_MIN)) + MONEY_MIN;
    }

//Accessors
    //Desc: returns m_name
    //Pre: none
    //Post: returns m_name
    string name() const {return(m_name);}

    //Desc: returns m_funds
    //Pre: none
    //Post: returns m_funds
    float money() const {return(m_funds);}

    //Desc: returns number of purchases
    //Pre: none
    //Post: returns m_p_num
    short bags () const {return m_p_num;}

    //Desc: returns specified item from m_purchases
    //Pre: m_purchases must contain an item at the specified location
    //Post: returns specified item from m_purchases
    product stuff(const short item) const {return (m_purchases[item]);}

    //Desc: returns inclination
    //Pre: none
    //Post: returns m_favorite
    bool fav() const {return (m_favorite);}

    //Desc: returns customer's happiness index
    //Pre: none
    //Post: returns m_vicodin
    short DrMonroe(){return(m_vicodin);}

//Mutators
    //Desc: changes number of purchases
    //Pre: if num is < 0 then m_p_num must be > 0
    //Post: adds num to m_p_num
    void addCart(const short num){m_p_num += num; return;}

    //Desc: Adds a customer
    //Pre: None
    //Post: Fills in name and inclination values for a customer object. 
    //      Assumes shop will be either 1 or -1.
    void add_cust(const string name, const int shop)
      {m_name = name; m_favorite = shop+1; return;}

    //Desc: adjusts happiness
    //Pre: none
    //Post: Adds bipolar to m_vicodin
    void life (short bipolar){m_vicodin += bipolar; return;}

//Other
    //Desc: Throws an item at victim if possible
    //Pre: none
    //Post: Throws an item if possible, adjusts happiness levels accordingly
    void chuck(customer & victim);

    //Desc: Robs victim if possible
    //Pre: none
    //Post: Robs victim if possible, adjusts happiness levels accordingly
    void rob(customer & victim);

    //Desc: displays customer's cart
    //Pre: none
    //Post: displays customers name, funds and current purchases
    friend ostream & operator << (ostream & stream, const customer & patsy);

    //Desc: Checks if customer wants to buy something, what they want to buy
    //      if they can buy it and then sells it to them (or not) and adjusts
    //      their happiness levels
    //Pre: none
    //Post: Checks if customer wants to buy "suggested",  if they can buy it 
    //      and then adjusts m_p_num, m_vicodin and m_purchases if 
    //      neccessary and returns result.
    bool buy_something(const product suggested);
};

#endif
