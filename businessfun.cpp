//Name:James Felber
//Section: D
//Date:11-6-2013
//Desc: function file for business class

#include "business.h"
#include "customer.h"


void business::display() const
{
  cout << m_name << endl
       << "Current Inventory: " << endl;
  for (int i = 0; i < m_variety; i++)
    cout << "\t" << m_stock[i].m_item << "\t $"
         << m_stock[i].m_price << endl;

  cout << "Cash on Hand: " << m_funds << endl;

  cout << "Customers:"<< endl;

  for (int i = 0; i < m_occupants; i++)
   {
    cout << "\t" << m_customer[i].name();
    cout << endl;
   }
  cout << " \n";
  
  return;
}

void business::AddCustomer(const customer & ncust)
{
  if (m_occupants < OCCUPANCY)
    {
      m_customer[m_occupants] = ncust;
      m_occupants++;
    }
  return;
}


void business::sell_stuff()
{
  product suggest;

  for (int i = 0; i<m_occupants; i++)
  {
    suggest = m_stock[rand()%m_variety];
    if (m_customer[i].buy_something(suggest))
    {
      m_funds += suggest.m_price;
//      cout << m_customer[i].name() << " bought "
//           << suggest.m_item << " for $"
//           << suggest.m_price << endl;
    }
  }

  return;
}

void business::customers_leave(customer sesame[], const short current)
{
  
  for (int i = 0; i < m_occupants; i++)
    sesame[i+current] = m_customer[i];
  
  m_occupants = 0;

  return;
}
